import sys

n = 7654321
while True:
    lis = []
    pan = True
    for digit in str(n):
        lis.append(int(digit))
    if lis.count(0) > 0:
        pan = False
    for i in range(1,9):
        if lis.count(i) != 1:
            pan = False
    if pan:
        print(n)
        prime = True
        for i in range(2,n):
            if n % i == 0:
                prime = False
                break
        if prime:
            print(n)
            sys.exit()
    n = n-2
                