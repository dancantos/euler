lis = []
with open("max_path_sum_1","r") as f:
    for line in f:
        lis.append(line.strip().split())

paths = []
for i in lis:
    paths.append([ int(s) for s in i ])

def children(list1, list2):
    return [ (list2[i],list2[i+1]) for i in range(len(list1)) ]

def max_pathing(list1, list2):
    return [ list1[i] + max(children(list1,list2)[i]) for i in range(len(list1)) ]

def max_path(paths):
    while len(paths) > 1:
        [list1,list2] = paths[-2:]
        del paths[-2:]
        paths.append(max_pathing(list1,list2))
    return paths[0]

print(max_path(paths)[0])
