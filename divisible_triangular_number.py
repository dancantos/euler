'''
Author: Daniel Cantos

task: to find the first triangle number with over 500 divisors

triangle numbers: integers of the form n(n+1)/2 for any integer n

solution uses prime factorisation and then simple counting methods to count how many numbers can be created
as multiple of those prime factors
'''

def prime_factors(n):
    i = 2
    lis = []
    while i < n:
        if n % i == 0:
            lis.append(i)
            n = n // i
            i = 2
        else:
            i += 1
    lis.append(n)
    return lis

def count_factors(lis):
    c = []
    while lis:
        c.append(lis.count(lis[0]))
        lis = list(filter((lis[0]).__ne__, lis))
    return(c)

def divisibility(n):
    counted = count_factors(prime_factors(n))
    res = 1
    for i in counted:
        res = res * (i+1)
    return res

for n in range(1000000):
    t = n*(n+1)//2
    if divisibility(t) > 500:
        print("the first triangle number with over 500 divisors is {} with {} divisors".format(t, divisibility(t)))
        break


    
    
    
    
    
    
    