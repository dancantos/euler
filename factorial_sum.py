def fact(n, p):
    if n < 2:
        return p
    return fact(n-1, n*p)

print(fact(5,1))

n = fact(100,1)
print(n)

s = 0
for i in str(n):
    s += int(i)

print(s)