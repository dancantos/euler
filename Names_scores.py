names = []
with open("names.txt","r") as f:
    for line in f:
        string = line

string = string.replace(',',' ').replace('"','')
names = string.split()
names = sorted(names)

def name_score(name):
    sum = 0
    for letter in name:
        sum += ord(letter)-64
    return sum

print(name_score("MARY"))

total = 0
for i in range(len(names)):
    total += (i+1)*name_score(names[i])

print(total)