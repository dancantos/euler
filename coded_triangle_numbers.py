import math

with open("words.txt","r") as f:
    string = ""
    for line in f:
        string += line

string = string.replace(',',' ').replace('"',"")
words = string.strip().split()
print(words)

def score(string):
    sum = 0
    for character in string:
        sum += (ord(character) - 64)
    return sum

t = 1
i = 2
triangle_nums = []
while t < 400:
    triangle_nums.append(t)
    t += i
    i += 1

print(triangle_nums)

def is_triangle(num):
    if num in triangle_nums:
        return True
    return False

print(is_triangle(55))

count = 0
for word in words:
    if is_triangle(score(word)):
        count += 1
print(count)