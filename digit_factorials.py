def fact(n, p):
    if n < 2:
        return p
    return fact(n-1, n*p)

lis = []
for i in range(3,2540160):
    ints = [ int(digit) for digit in str(i) ]
    sum = 0
    for num in ints:
        sum += fact(num,1)
    if sum == i:
        print(i)
        lis.append(i)

print(lis)