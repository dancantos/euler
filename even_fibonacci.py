import math


def prime_factors(n):
    i = 2
    lis = []
    while i < n:
        if n % i == 0:
            lis.append(i)
            n = n // i
            i = 2
        else:
            i += 1
    lis.append(n)
    return lis

for i in range(20):
    print(prime_factors(i))

print(prime_factors(232792560))

def count_same_factors(lis):
    c_list = []
    for num in lis:
        c_list.append(lis.count(num))
        c_list = (lambda a: a != num, c_list)
    return c_list

print(count_same_factors([2,2,2,3,3,5]))