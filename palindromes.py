def is_pal(string):
    lis = []
    for character in string:
        lis.append(character)
    if len(string) % 2 == 0:
        return string[:len(string)//2] == string[len(lis)//2:][::-1]
    return string[:len(string)//2] == string[len(lis)//2+1:][::-1]

def is_even(num):
    return num % 2 == 0

def satisfies(num):
    b = bin(num)[2:]
    s = str(num)
    return is_pal(b) and is_pal(s)

test_list = []
#for i in range(1000000):
#    if not is_even(i):
#        if satisfies(i):
#            test_list.append(i)

test_list = [1, 3, 5, 7, 9, 33, 99, 313, 585, 717, 7447, 9009, 15351, 32223, 39993, 53235, 53835, 73737, 585585]

sum = 0
for num in test_list:
    sum += num
print(sum)