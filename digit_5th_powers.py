lis = []
for i in range(2, 354294):
    ints = [ int(digit) for digit in str(i) ]
    sum = 0
    for num in ints:
        sum += num**5
    if sum == i:
        print(i)
        lis.append(i)

answer = 0
for i in lis:
    answer += i
print(answer)