def factor_gen(n):
    yield 1
    for i in range(2, n//2+1):
        if n % i == 0:
            yield i

abundant_numbers = []
with open("abundant_numbers","r") as f:
    for line in f:
        abundant_numbers.append(int(line))

print(abundant_numbers)
abundant_sums = []

for i in abundant_numbers:
    for j in abundant_numbers:
        if i + j < 28123:
            if i + j not in abundant_sums:
                abundant_sums.append(i+j)

print(abundant_sums)