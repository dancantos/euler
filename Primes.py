import math

lis = [2, 3]

def append_prime_list():
    i = lis[-1:][0] + 2
    while True:
        for p in lis:
            if i % p == 0:
                break
            if p == lis[-1:][0]:
                lis.append(i)
                return
        i += 2
        
#for i in range(9999):
#    append_prime_list()
#print(lis)
#print(lis[-1:][0])

def is_prime(p):
    for i in range(2,math.floor(math.sqrt(p))+1,1):
        if p % i == 0:
            return 0
        i += 1
    return p

print(is_prime(97))