n = 2
def prime_list_gen(n):
    for i in range(2,n):
        prime = True
        for j in range(2, i):
            if i % j == 0:
                prime = False
                break
        if prime:
            yield i

print(list(prime_list_gen(10)))