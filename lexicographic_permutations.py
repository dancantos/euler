import math

def fact(n,p):
    if n < 2:
        return p
    return fact(n-1, p*n)

def dec_to_fact(n):
    i = 0
    while n >= fact(i,1):
        i += 1
    i -= 1
    lis = []
    while i >= 0:
        f = fact(i,1)
        d = math.floor(n/f)
        yield d
        r = n % f
        i -= 1
        n = r

def permute(lis, n):
    fact_num = list(dec_to_fact(n-1))
    permuted_list = []
    while len(fact_num) < len(lis):
        fact_num.insert(0,0)
    while len(fact_num) > 0:
        index = fact_num.pop(0)
        element = lis.pop(index)
        permuted_list.append(element)
    return permuted_list
    
def list_to_string(lis):
    string = ""
    for character in lis:
        string += str(character)
    return string

def list_sum(lis):
    sum = 0
    for num in lis:
        sum += num
    return sum

def fact_sum(n):
    return list_sum(dec_to_fact(n))

def dec_string(n):
    string = "("
    string += str(n)
    string += ")_10"
    return string

def fact_string(n):
    string = "("
    for num in list(dec_to_fact(n)):
        string += str(num)
    string += ")_!"
    return string

for i in range(24):
    print(dec_string(i) + "\t\t" + fact_string(i) + "\t\t" + str(fact_sum(i)))

