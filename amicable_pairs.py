'''
Author: Daniel Cantos

task: find the sum of all amicable numbers below 10000

amicable numbers: let d(x) be the sum of all the proper divisors of an integer x
2 numbers x and y are an amicable pair iff d(x) = y and d(y) = x but y =/= x
note: this is equivalent to saying that x is amicable iff d(d(x)) = x but x =/= d(x)
'''

import math

def factor_gen(n):
    yield 1
    for i in range(2, n//2+1):
        if n % i == 0:
            yield i

def factor_sum(n):
    '''
    computes d(n)
    '''
    s = 0
    for num in factor_gen(n):
        s += num
    return s

lis = []
i = 1
while i < 10000: #finds the amicable numbers less than 10000
    d = factor_sum(i)
    if i == factor_sum(d) and i != d:
        lis.append(i)
        lis.append(d)
        i = d + 1
    i += 1

print("amicable numbers less than 10000 are {}".format(lis))
result = 0
for i in lis:
    result += i

print("sum is {}".format(result))
        