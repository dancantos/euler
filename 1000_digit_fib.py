'''
Author: Daniel Cantos

task: to find the first fibonacci number with over 1000 digits
'''

n = 0
i = 1
j = 1
count = 2
while True:
    temp = j
    j += i
    i = temp
    count += 1
    if len(str(j)) == 1000:
        print(count)
        break
    
    